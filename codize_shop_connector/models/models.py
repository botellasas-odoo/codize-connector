# -*- coding: utf-8 -*-

from odoo import models, fields, api

class CodizeShopAnalytics(models.Model):
    _name = 'codize_shop.analytics'

    name = fields.Char('Name')
    date = fields.Date('Date')
    user_id = fields.Many2one('res.users', string='User')
    url = fields.Char('URL')
    product_id = fields.Many2one('product.template', string='Product')