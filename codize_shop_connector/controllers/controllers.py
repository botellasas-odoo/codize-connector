# -*- coding: utf-8 -*-
from odoo import http
from odoo.http import request
import re

class CodizeShopConnector(http.Controller):

    @http.route('/codize/shop/login', type="json", auth='none', cors='*')
    def codize_login(self, **kw):
        try:
            uid = request.session.authenticate(kw['db'], kw['login'], kw['password'])
            if uid:
                user = request.env['res.users'].search([('id', '=', uid)])
                priceslist = request.env['product.pricelist'].search([('id', '=', user.partner_id.property_product_pricelist.id)])

                #print('>>>>>>>>>>>>>>>>>>>>>>>>')
                #print(priceslist.item_ids[0].fixed_price)

                return {
                    'status': True,
                    'user': user.id,
                    'partner': user.partner_id.id,
                    'partner_name': user.partner_id.name,
                    'partner_img': user.partner_id.image,
                    'address': {
                        'country_id': user.partner_id.country_id.name,
                        'state_id': user.partner_id.state_id.name,
                        'city': user.partner_id.city,
                        'street': user.partner_id.street
                    }
                }
        except:
            return {'status': False}

    @http.route('/codize/shop/editaddress', type="json", auth='none', cors='*')
    def codize_edit_address(self, **kw):
        try:
            uid = request.session.authenticate(kw['db'], kw['login'], kw['password'])
            if uid:
                user = request.env['res.users'].search([('id', '=', uid)])
                partner = request.env['res.partner'].search([('id', '=', user.partner_id.id)])
                edit = partner.write({
                    'street': kw['street'],
                    'city': kw['city']
                })
                if edit:
                    return {'status': True}
                else:
                    return {'status': False}
        except:
            return {'status': False}

    @http.route('/codize/shop/create', type="json", auth='none', cors='*')
    def codize_create_sale(self, **kw):
        orderLine = []

        if kw['order_line']:
            for line in kw['order_line']:
                orderLine.append((0, 0, {
                    'product_id': line['pid'],
                    'product_uom_qty': line['qty']
                }))

        if kw['partner_id']:
            saleorder = request.env['sale.order'].sudo().create({
                'partner_id': kw['partner_id'],
                'pricelist_id': 1,
                'order_line': orderLine
            })
            return {
                'name': saleorder.name,
                'status':"Sale Order Create from Codize Shop"
            }
        else:
            return {'status': "Error"}

    @http.route('/codize/products/count', type="json", auth='none', cors='*')
    def codize_count_products(self, **kw):
        products = request.env['product.template'].search(kw['filter'])
        return len(products)

    @http.route('/codize/products', type="json", auth='none', cors='*')
    def codize_get_products(self, **kw):
        rp = []
        products = request.env['product.template'].search(kw['filter'], limit=20, offset=kw['off'], order=kw['order'])
        for product in products:
            if product.website_style_ids:
                rp.append({
                   'name': product.name,
                    'id': product.id,
                    'image': product.image_medium,
                    'price': product.lst_price,
                    'currency': product.currency_id.symbol,
                    'styles': product.website_style_ids[0].html_class
                })
            else:
                rp.append({
                   'name': product.name,
                    'id': product.id,
                    'image': product.image_medium,
                    'price': product.lst_price,
                    'currency': product.currency_id.symbol,
                    'styles': False
                })
        return rp

    @http.route('/codize/products/categories', type="json", auth='none', cors='*')
    def codize_get_categories(self, **kw):
        rc = []
        categories = request.env['product.public.category'].search(kw['filter'])
        for category in categories:
            rc.append({
                'name': category.name,
                'id': category.id
            })
        return rc

    @http.route('/codize/product', type="json", auth='none', cors='*')
    def codize_get_product(self, **kw):
        rp = []
        images = []
        variants = []
        product = request.env['product.template'].sudo().search(kw['filter'])
        pp = 0

        if product.product_image_ids:
            for i in product.product_image_ids:
                images.append(i.image)

        if product.attribute_line_ids:
            values = []
            for i in product.attribute_line_ids:
                vals = []

                for v in i.value_ids:
                    vals.append({
                        'name': v.name,
                        'id': v.id
                    })

                variants.append({
                    'name': i.attribute_id.name,
                    'id': i.id,
                    'values': vals,
                    'selected': i.value_ids[0].name,
                    'sel_val_id': i.value_ids[0].id
                })
                values.append(i.value_ids[0].id)

            pp = request.env['product.product'].search([('product_tmpl_id', '=', product.id)])
            for v in values:
                pp = pp.search([('attribute_value_ids', 'in', v)])
        else:
            pp = request.env['product.product'].search([('product_tmpl_id', '=', product.id)], limit=1)

        if product.taxes_id:
            taxes = product.taxes_id[0].name
            per_taxes = product.taxes_id[0].amount
        else:
            taxes = False
            per_taxes = 0

        rp.append({
            'name': product.name,
            'id': product.id,
            'pid': pp.id,
            'image': product.image,
            'image_small': product.image,
            'images': images,
            'price': product.lst_price,
            'currency': product.currency_id.symbol,
            'description_sale': product.description_sale,
            'qty_available': product.qty_available,
            'uom_name': product.uom_id.name,
            'variants': variants,
            'tax': taxes,
            'per_taxes': per_taxes
        })
        return rp

    @http.route('/codize/product/getid', type="json", auth='none', cors='*')
    def codize_get_productid(self, **kw):
        product = request.env['product.template'].sudo().search(kw['filter'], limit=1)

        if product:
            return {
                'status': True,
                'id': product.id
            }
        else:
            return {'status': False}

    @http.route('/codize/search/product/att', type="json", auth='none', cors='*')
    def codize_product_att(self, **kw):
        values = kw['values']
        pp = request.env['product.product'].search([('product_tmpl_id', '=', kw['pid'])])
        for v in values:
            pp = pp.search([('attribute_value_ids', 'in', v)])

        return {
            'price': pp.lst_price,
            'pid': pp.id
        }

    @http.route('/codize/shop/lead', type="json", auth='none', cors='*')
    def codize_lead(self, **kw):
        request.env['crm.lead'].sudo().create({
            'partner_id': kw['partner_id'],
            'contact_name': kw['contact_name'],
            'email_from': kw['email_from'],
            'name': kw['name'],
            'description': kw['description']
        })

        return 'Lead was generated'

    @http.route('/codize/shop/news', type="json", auth='none', cors='*')
    def codize_news(self, **kw):
        n = []

        blog = request.env['blog.blog'].sudo().search([('name', '=', 'Codize')], limit=1)
        blogs = request.env['blog.post'].sudo().search([('blog_id', '=', blog.id),('is_published', '=', True)], limit=3)
        for b in blogs:
            n.append({
                'id': b.id,
                'name': b.name,
                'subtitle': b.subtitle,
                'background': re.search('url\((.*?)\)', b.cover_properties).group(1)
            })
        return n
    
    @http.route('/codize/shop/new', type="json", auth='none', cors='*')
    def codize_new(self, **kw):
        blog = request.env['blog.post'].sudo().search([('id', '=', kw['id'])], limit=1)

        if blog:
            return {
                'name': blog.name,
                'subtitle': blog.subtitle,
                'content': blog.content
            }
        else:
            return 'No blog'

    @http.route('/codize/saleorder', type="json", auth='none', cors='*')
    def codize_sale_order(self, **kw):
        try:
            uid = request.session.authenticate(kw['db'], kw['login'], kw['password'])
            saleorders = []
            if uid:
                user = request.env['res.users'].search([('id', '=', uid)])
                so = request.env['sale.order'].search([('partner_id', '=', user.partner_id.id)], limit=20, order='date_order desc')
                for s in so:
                    l = []
                    for lines in s.order_line:
                        l.append({
                            'name': lines.product_id.name
                        })
                    saleorders.append({
                        'name': s.name,
                        'date': s.date_order,
                        'lines': l,
                        'total': s.amount_total,
                        'currency': s.currency_id.symbol
                    })
                return {
                    'so': saleorders,
                    'status': True
                }
        except:
            return {'status': False}
