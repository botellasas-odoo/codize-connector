# -*- coding: utf-8 -*-
#################################################
# Develop for Codize (www.codize.ar)
# Main dev: Ignacio Buioli <ibuioli@gmail.com>
#
# This Odoo Module and its Components
# are part of Codize Engine, they cannot be 
# modified without the corresponding license.
#
# © Codize
#################################################
{
    'name': "codize_connector",

    'summary': """
     Codize connector License""",

    'description': """
        Codize connector License
    """,

    'author': "Codize",
    'website': "www.codize.ar",

    'category': 'Uncategorized',
    'version': '0.1',

    'depends': ['base', 'sale'],

    'data': [
        'views/views.xml',
        'views/templates.xml',
    ],
    'demo': [
        'demo/demo.xml',
    ],
}
