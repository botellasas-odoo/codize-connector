# -*- coding: utf-8 -*-

from odoo import models, fields, api

class CodizeConnector(models.Model):
	_inherit = 'res.users'

	cz_license = fields.Char('CZ License')
	cz_user_num = fields.Integer('CZ User Number')

class SaleOrder(models.Model):
	_inherit = 'sale.order'

	@api.multi
	def geo_localize(self):
		for rec in self:
			data = str(rec.partner_latitude) + '+' + str(rec.partner_longitude)

		client_action = {
                        'type': 'ir.actions.act_url',
                        'name': "GeoLoc",
                        'target': 'new',
                        'url': 'https://www.google.com/maps?&z=15&q=' + data,
		}
		return client_action

	partner_latitude = fields.Float(string='Geo Latitude', digits=(16, 5))
	partner_longitude =  fields.Float(string='Geo Longitude', digits=(16, 5))

	signed_on = fields.Date(string='Signed On')
